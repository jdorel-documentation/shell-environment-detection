# Specific shells notes

## Shell

### Login shell

ZSH: [[ -o login ]]

FISH: if status --is-login

TCSH: if($?loginsh) then

### Interactive shell

FISH: if status --is-interactive

CSH/TCSH: if($?prompt) then

Check for a prompt [[ -z $PS1 ]]

POSIX: s in $-

### User vs System (CRON) ?

[[ TERM=="dumb" ]] && echo "Running in cron

## Specific tty

## Xorg

## SSH

